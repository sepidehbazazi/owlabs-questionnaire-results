from __future__ import print_function
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from wordcloud import WordCloud

import configurations

# Further details here: https://developers.google.com/sheets/api/quickstart/python
# pip install --upgrade google-api-python-client oauth2client
# If modifying these scopes, delete the file token.json.
SCOPES = configurations.SCOPES

# The ID and range of a sample spreadsheet.
SPREADSHEET_ID = configurations.SPREADSHEET_ID
RANGE_NAME = configurations.RANGE_NAME

def make_plot(table, col):
    """
    Create a bar plot of % answers for different columns in the questionnaire answer's table
    Unknown, NA, and None answers are excluded from the calculation.
    :param table: questionnaire table
    :param col: column of interest
    :return:
    """
    plt.figure(figsize=(10,10))
    y_value = table[col].dropna().drop(table[table[col]=='NA'].index).value_counts(normalize=True).round(2)*100
    print(y_value)
    y_pos = np.arange(len(y_value))
    objects = y_value.index
    plt.bar(y_pos, y_value, align='center', alpha=0.5)
    plt.xticks(y_pos, objects, fontsize=20, rotation=45)
    plt.yticks(range(0,110,10), fontsize=20,)
    plt.ylabel('% responses', fontsize=20)
    plt.title(table[col].name, fontsize=20)
    plt.tight_layout()
    plt.savefig('plots/{}.png'.format(table[col].name))


def make_wordcloud(table,col):
    """
    Create a word cloud from column in table
    :param table: questionnaire table
    :param col: column of interest
    :return:
    """
    text = " ".join(st for st in table[col])
    wordcloud = WordCloud(max_font_size=50, max_words=100, background_color="white").generate(text)
    plt.figure()
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis("off")
    plt.savefig('plots/{}.png'.format(table[col].name))


def main():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    store = file.Storage('token.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
        creds = tools.run_flow(flow, store)
    service = build('sheets', 'v4', http=creds.authorize(Http()))

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                                range=RANGE_NAME).execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')
    else:
        print('Name, Major:')
        table=[]
        for row in values:
            table.append(row)

    table = pd.DataFrame(table, columns=table[0]).drop(0).dropna(how='all')
    for col in configurations.cols_to_plot:
        make_plot(table, col)

    for col in configurations.wordcloud_cols:
        make_wordcloud(table,col)

    for col in configurations.text_cols:
        print(col)
        print(table[col].dropna().drop(table[table[col]=='NA'].index).values)

if __name__ == '__main__':
    main()