# Google sheet - data source specifications
# If modifying these scopes, delete the file token.json.
SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'

# The ID and range of a sample spreadsheet.
SPREADSHEET_ID = '1qsL3DFcFZbSjkMrGzn1srMI6aOikT7QeObIIdnuy4Mc'  # This can be found in the Google Sheet URL
RANGE_NAME = 'cleanData!A:N'  #The worksheet and columns to include

# Analysis details
# A list of questionnaire column names (categories) for which the % answers is to be plotted
cols_to_plot = ['Company type', 'Interesting work (vs. Labs)?',
                           'Role seniority (benchmarked to OW levels)',
                           'Top tier OWLabs type role?', 'Hours','Travel (%)', 'Weekend work?',
                            'Is family life supported?',
                           'Stress (vs. Labs)?',
                            'Company size (employees)', 'Company size (revenue)',
                            'Role'

                           ]

#  A list of questionnaire column names for which a word cloud will be plotted
wordcloud_cols = ['Company type']

#  A list of questionnaire column names for which the responses will be printed in the terminal
text_cols =['Company type', 'Company size (employees)', 'Company size (revenue)', 'Package', 'What else?']