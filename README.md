# Google Sheet Questionnaire Analysis
##### Note: the google sheet should not contain any confidential or sensitive information
This project is designed to get started analysing the results of a Google Sheet questionnaire. 
The goal is to produce plots summarizing question results.


## Setup

Currently the application is build on a Python foundation.

Firstly, you will need to enable the google sheets API and download the credentials.json file
- Visit: https://developers.google.com/sheets/api/quickstart/pythonTypeError: expected string or bytes-like object

To get started, we recommend building a new Python virtual environment, and installing all of the
requirements in the `requirements.txt` file.

To build a new virtual environment, run:
- Install virtualenv package: `pip3 install virtualenv`
- Create a virtual environment `virtualenv -p python3 venv`
- To activate the environment run `source venv/bin/activate`
- Install the requirements: `pip install -r requirements.txt`
- Configure the configurations.py file with the SCOPES, SPREADSHEET_ID, RANGE_NAME, columns to plot, wordcloud_cols

To run the code, you will need to 'activate' the virtual environment for every session.

## Running
In the terminal window, run the code:
- `python start.py`

### Outputs
Plots can be found in the plots folder.
Text outputs will appear in the terminal window

